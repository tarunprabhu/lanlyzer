#include <stdio.h>
#include <stdlib.h>
#include <math.h>

static const int def_m = 8;
static const int def_n = 8;

void print(int* arr, int m, int n) {
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
      printf("%d ", arr[i * n + j]);
    }
    printf("\n");
  }
}

int main(int argc, char* argv[]) {
  int m = argc > 1 ? def_m : atoi(argv[1]);
  int n = argc > 2 ? def_n : atoi(argv[2]);

  int* arr = (int*)malloc(sizeof(int) * m * n);
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
      arr[i * n + j] = random();
    }
  }

  print(arr, m, n);

  return 0;
}
