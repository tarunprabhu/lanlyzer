## Lanylzer

Uninspired name aside, this is a collection of LLVM passes and build scripts that
are intended to be a reference for writers of out-of-tree LLVM IR passes. The 
passes themselves simply print out some things to show that they are doing
"something". The primary goal here is to document how to write, build
and run an LLVM pass without having to add the pass into LLVM's code base or 
modify LLVM itself. There is a fair bit of un-intuitive boilerplate and other 
machinery that needs to be set up --- while this is documented in places within
LLVM, it is not always easy to find. 

The code in this repository is what is important - not what that code does. 
It has, therefore, been commented more extensively than it normally be. This is
particularly true for those places where some decision was made but for which 
other options exist. It is important therefore to read the comments in addition
to using the code as a reference. In places, they provide hints to alternatives 
for what can be done depending on your needs. Of course, LLVM's own documentation
is the final authority on such matters and you should make a habit of consulting
it  frequently. While it is quite good as these things go, it can still be
somewhat overwhelming for newcomers and has even been known to confound old hands
on occasion. Hopefully, the inline code comments will give you a better idea of
what to look for in the official documentation. It may also provide you with some
background to better understand what is written there.

LLVM is large and fairly complex (as one would expect a high-quality,
industrial-strength compiler to be). There are many things that one can do with
LLVM passes and we have attempted to provide a taste of those. We also tried to
keep each pass somewhat short. As as result, some of the "useful" information is
spread out among the various passes. We would recommend at least skimming the 
code of all of the passes just to get a sense of what is possible with LLVM.


### Building

The pre-requisites to build `lanlyzer` are:

    - `CMake` (version 3.25 or higher).
    - C++ compiler with C++17 support.
    - `LLVM`.
    - `Make`, `Ninja` or some other build system supported by `CMake`.

`lanlyzer` does not need to be compiled with clang. LLVM does _not_ have to be 
built from source either, although you are free to do so if you wish. Most major
Linux/Unix distributions should provide pre-built binaries of LLVM. Some package
managers provide several different versions of LLVM. Ensure that you obtain one 
of the supported versions (see below). Building on Windows is currently not
supported.

The following LLVM versions are currently supported:

    - 15.x

LLVM's API can and often does change with every release. There is no 
guarantee that this code will compile with an unsupported version of LLVM.

Building in the same directory as the source is discouraged. The high-level 
steps to build `lanlyzer` are as follows:

```
    $ Checkout/extract lanlyzer to $PWD/lanlyzer
    $ mkdir build
    $ cd build
    $ cmake -DWITH_LLVM=/path/to/llvm/install/root ../lanlyzer/CMakeLists.txt
    $ make
```

The `WITH_LLVM` flag is optional. If it is not provided, `cmake` will look for
LLVM in the standard search paths for the platform as well as anything added to 
`CMAKE_PREFIX_PATH`. If you have multiple LLVM installations on your system, 
the `WITH_LLVM` flag is strongly recommended.


### Running the passes

Once built, each pass will be in dynamic shared object (`.so` file on Linux/*BSD,
`.dyn` on MacOSX) in the build directory. To run a given pass, use the following
command-line flags with clang.

```
    $ clang -Xclang -disable-O0-optnone -fpass-plugin=/path/to/<pass-name>.so <file>
```

clang automatically  adds an `optnone` attribute to all functions when compiled 
without optimizations (`-O0`). Passes that operate on functions and loops will
ignore any functions with this attribute. The `-Xclang -disable-O0-optnone` 
flags stop clang from adding the `optnone` attribute to functions when
compiling without optimizations. An alternative would be to compile with `-O1`
but that may not be ideal (for instance, you may want the pass to run on 
unoptimized code).


### Current status and limitations

The eventual goal is to have all of LLVM's major pass categories represented, but
for now, the three major pass types are present:

    - Module passes that operate on the LLVM module.
    - Function passes that operate on a LLVM function at a time.
    - Loop passes that operate on loop nests.
    
Additionally, the passes are all analysis passes, which are simpler than 
transformational passes. Since the former do not modify the IR, they do not 
invalidate other existing analyses, so there is no need to inform LLVM's
`PassManager` of such things (some transformational passes may need
to keep analyses up-to-date while they are running which is an additional 
complication).
