// Tarun Prabhu [02-Feb-2023]

#include "llvm/Analysis/LoopInfo.h"
#include "llvm/Analysis/ScalarEvolution.h"
#include "llvm/Passes/PassBuilder.h"
#include "llvm/Passes/PassPlugin.h"
#include "llvm/Support/raw_ostream.h"
#include <llvm/Transforms/Scalar/LoopPassManager.h>

using namespace llvm;

static const char *passPipelineName = "lanlyzer-loops";
static const char *passName = "Lanlyzer::Loops";

namespace lanlyzer {

class LoopAnalyzerPass : public PassInfoMixin<LoopAnalyzerPass> {
private:
  // This is an example of a utility function that may need to be written when
  // LLVM's API misses the odd thing (of course, it could be that they have
  // added an equivalent of this method and I have missed it - new stuff gets
  // added all the time, so the official docs are your friend).
  Function &getFunctionContainingLoop(Loop &loop) {
    // LLVM's loops have a whole terminology associated with them. It's not
    // entirely non-standard and you will see the same in the literature, but
    // it is best to read up on it: https://llvm.org/docs/LoopTerminology.html.

    // Well-formed LLVM loops are guaranteed to have a header. Even if the loop
    // consists of a single basic block, that block will be considered the
    // header.
    BasicBlock *header = loop.getHeader();

    return *header->getParent();
  }

public:
  static StringRef name() { return passName; }

  // Note that the signature of the run method for a loop pass is very
  // different from that of a function or module pass.
  PreservedAnalyses run(LoopNest &nest, LoopAnalysisManager &,
                        LoopStandardAnalysisResults &lar, LPMUpdater &) {
    errs() << "Found loop nest: " << nest;

    // Apparently, this is a new change to LLVM that I wasn't aware and now
    // they have a LoopNest object and you have to do the following to get the
    // loop. Will wonders never cease!
    Loop &loop = nest.getOutermostLoop();

    // When writing an analysis pass, it may be useful to have the results of
    // other analysis passes. For instance, to determine the increment of the
    // loop induction variable, one could write a helper routine to do so.
    // However, LLVM's ScalarEvolution analysis can often provide this
    // information, and may also be able to handle loops where the evolution
    // of the induction variable is obscured. The LoopAnalysisManager can be
    // queried to obtain these analysis results. The FunctionAnalysisManager
    // and ModuleAnalysisManager can also be used in a similar fashion for
    // Function and Module analyses respectively.
    //
    ScalarEvolution &se = lar.SE;

    if (PHINode *phi = loop.getInductionVariable(se))
      errs() << "Loop has an induction variable: " << *phi << "\n";
    else
      errs() << "No induction variable in loop\n";

    // The loop information is spread between the Loop object itself (which
    // inherits from LoopBase) and the LoopInfo object. Depending on what you
    // are trying to do, you may need one or both.
    LoopInfo &li = lar.LI;

    // Usually, just looking at the loop structure isn't enough. One typically
    // needs to look at the instructions as well and also where the loop fits
    // within the parent function (as opposed to any parent loops that it might
    // have). There is more information on iterating over Instructions and the
    // like in the FunctionAnalyzer.cpp file.

    return PreservedAnalyses::all();
  }
};

} // namespace lanlyzer

// The callback to parse the pass name when using opt. With this, one can say
//
//     opt --passes="(lanlyzer-loops)"
//
static bool passParsePipeline(StringRef name, LoopPassManager &lpm,
                              ArrayRef<PassBuilder::PipelineElement>) {
  if (name == passPipelineName) {
    lpm.addPass(lanlyzer::LoopAnalyzerPass());
    return true;
  }
  return false;
}

// The callback to register the pass with the pass manager. The precise
// signature of this callback will vary depending on with PassBuiler::register*
// method you use (see `registerPass` below). In this particular case, we are
// adding the pass early in the optimization pipeline. The OptimizationLevel
// argument is ignored since this is an analysis pass and we want it to always
// run. But for an optimization pass, this could be used to enable it only at
// certain optimization levels.
static void passRegisterWithPassManager(ModulePassManager &mpm,
                                        OptimizationLevel) {
  // This needs the loop pass to be converted into a module pass, so there's
  // two levels of adaptor creation here.
  mpm.addPass(createModuleToFunctionPassAdaptor(
      createFunctionToLoopPassAdaptor(lanlyzer::LoopAnalyzerPass())));
}

// This callback is used to register the pass with the PassBuilder to allow it
// to be used in different ways. In this case, we want to be able to run it from
// with opt using ```opt --passes```, but we also want it to run automatically
// when passed to clang/flang with -fpass-plugin. There are other register
// methods in the PassBuilder that can be used to run the pass at different
// stages in the optimization pipeline. OptimizerEarlyEP is probably not the
// best place, so you should really take a look at the options to see if there
// is a better point at which this pass ought to run.
static void registerPass(PassBuilder &pb) {
  pb.registerPipelineParsingCallback(passParsePipeline);
  pb.registerOptimizerEarlyEPCallback(passRegisterWithPassManager);
}

// This is the main magic incantation that allows the pass to work automatically
// once the shared object into which this is built is loaded. The load will
// be done either by -fpass-plugin in the case of clang/flang or -load with opt.
// It is a good idea to design the pass so that it can work with both tools.
extern "C" ::llvm::PassPluginLibraryInfo llvmGetPassPluginInfo() {
  return {LLVM_PLUGIN_API_VERSION, passName, LLVM_VERSION_STRING, registerPass};
}
