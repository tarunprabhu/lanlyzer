// Tarun Prabhu [02-Feb-2023]

#include "llvm/Passes/PassBuilder.h"
#include "llvm/Passes/PassPlugin.h"
#include "llvm/Support/raw_ostream.h"
#include <llvm/IR/PassManager.h>

using namespace llvm;

static const char *passPipelineName = "lanlyzer-module";
static const char *passName = "Lanlyzer::Module";

namespace lanlyzer {

// This is the new-style pass. There's no point in bothering about the legacy
// pass manager.
class ModuleAnalyzerPass : public PassInfoMixin<ModuleAnalyzerPass> {
public:
  static StringRef name() { return passName; }

  PreservedAnalyses run(Module &m, ModuleAnalysisManager &) {
    errs() << "Lanlyzer::module\n";

    // In general, the different pass types (module, function, cfg, loop) are
    // intended to be used for different transformations. If you know that a
    // particularly optimization can only be performed within a function,
    // for instance dead-code elimination, it should be implemented as a
    // function pass. For control-flow graph transformations, a cfg pass should
    // be preferred, even though one could carry out the same transformation
    // with a function pass. There used to be a restriction that function
    // passes were not allowed to modify anything outside a function, but I am
    // not sure if that restriction still holds. It can of course happen that
    // a transformation that ought to be a loop pass is more easily implemented
    // as a function pass, so the choice really does depend on what you are
    // trying to do.
    //
    // For analysis passes, it matters somewhat less which style of pass you
    // use since no transformations will be carried out on the code. However,
    // the choice of pass may affect the other analysis passes that become
    // available. For instance, I am not sure if one can get ScalarEvolution
    // results in a module pass (but I really don't know for certain, so you
    // should look this up).
    unsigned defined = 0;
    unsigned declared = 0;
    for (Function &f : m.functions()) {
      if (f.size())
        ++defined;
      else
        ++declared;
    }

    errs() << "declared: " << declared << "\n";
    errs() << "defined: " << defined << "\n";

    return PreservedAnalyses::all();
  }
};

} // namespace lanlyzer

// The callback to parse the pass name when using opt. With this, one can say
//
//     opt --passes="(lanlyzer-module)"
//
static bool passParsePipeline(StringRef name, ModulePassManager &mpm,
                              ArrayRef<PassBuilder::PipelineElement>) {
  if (name == passPipelineName) {
    mpm.addPass(lanlyzer::ModuleAnalyzerPass());
    return true;
  }
  return false;
}

// The callback to register the pass with the pass manager. The precise
// signature of this callback will vary depending on with PassBuiler::register*
// method you use (see `registerPass` below). In this particular case, we are
// adding the pass early in the optimization pipeline. The OptimizationLevel
// argument is ignored since this is an analysis pass and we want it to always
// run. But for an optimization pass, this could be used to enable it only at
// certain optimization levels.
static void passRegisterWithPassManager(ModulePassManager &mpm,
                                        OptimizationLevel) {
  mpm.addPass(lanlyzer::ModuleAnalyzerPass());
}

// This callback is used to register the pass with the PassBuilder to allow it
// to be used in different ways. In this case, we want to be able to run it from
// with opt using ```opt --passes```, but we also want it to run automatically
// when passed to clang/flang with -fpass-plugin. There are other register
// methods in the PassBuilder that can be used to run the pass at different
// stages in the optimization pipeline. OptimizerEarlyEP is probably not the
// best place, so you should really take a look at the options to see if there
// is a better point at which this pass ought to run.
static void registerPass(PassBuilder &pb) {
  pb.registerPipelineParsingCallback(passParsePipeline);
  pb.registerOptimizerEarlyEPCallback(passRegisterWithPassManager);
}

// Often, the registerPass and pass* functions are not written as separate
// functions but as lambdas in the object returned below. How you choose to do
// it is a matter of style. I have separated them out because I needed to
// comment them. But I would probably prefer the more compact lambda
// representation in practice because that code will seldom be modified after it
// has been written.
extern "C" ::llvm::PassPluginLibraryInfo llvmGetPassPluginInfo() {
  return {LLVM_PLUGIN_API_VERSION, passName, LLVM_VERSION_STRING, registerPass};
}
