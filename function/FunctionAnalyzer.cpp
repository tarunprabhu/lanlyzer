// Tarun Prabhu <tarun@lanl.gov> [02-Feb-2023]

#include "llvm/IR/InstIterator.h"
#include "llvm/IR/InstVisitor.h"
#include "llvm/IR/Instruction.h"
#include "llvm/IR/Instructions.h"
#include "llvm/Passes/PassBuilder.h"
#include "llvm/Passes/PassPlugin.h"
#include "llvm/Support/raw_ostream.h"

// There is a religious debate on whether "using namespace <some-large-ns> is a
// good idea. I think at some point, when Moya became quite large, it did
// become a problem and I stopped using this, but for now, it should be ok. Just
// keep in mind that if this project grows large, this decision may need to be
// revisited/
using namespace llvm;

static const char *passPipelineName = "lanlyzer-functions";
static const char *passName = "Lanlyzer::Function";

// The pass doesn't strictly need to be in a namespace - I think the in-tree
// LLVM passes are usually in an anonymous namespace and that is the pattern
// followed by the out-of-tree passes as well. I prefer to put my passes in
// a namespace especially when there is a chance that I will be working on it
// for a reasonable length of time and if there is a likelihood that I will be
// using external C++ libraries. In those cases, it just helps to keep things
// separated.
namespace lanlyzer {

// The instruction visitor class. The full documentation for the class is here:
// https://llvm.org/doxygen/classllvm_1_1InstVisitor.html. You only need to
// implement the methods for the instruction types that you are about.
class MyInstVisitor : public InstVisitor<MyInstVisitor> {
public:
  void visitLoadInst(LoadInst &load) {
    errs() << "Visited a load: " << load << "\n";
  }

  void visitStoreInst(StoreInst &store) {
    errs() << "Visited a store: " << store << "\n";
  }
};

// This is the new-style pass. There's no point in bothering about the legacy
// pass manager.
class FunctionAnalyzerPass : public PassInfoMixin<FunctionAnalyzerPass> {
public:
  static StringRef name() { return passName; }

  // This is the main method where the pass is run. It is not uncommon to have
  // this be a very thin wrapper that simply calls the run method on an Impl
  // class. I think that pattern is generally used when the class object needs
  // to be exposed through a header file. I am not sure that anyone ever does
  // that these days, so it's your choice on where to put the bulk of the
  // implementation.
  PreservedAnalyses run(Function &f, FunctionAnalysisManager &) {
    errs() << "Lanlyzer::function on " << f.getName() << "\n";

    // The "usual" way of iterating over a function is to iterate over all
    // basic blocks in the function and then on each instruction within the
    // basic block:
    //
    //     for(BasicBlock &bb : f) {
    //       for (Instruction& i : bb) {
    //         // Do whatever here.
    //       }
    //     }
    //
    // If you don't really care about the basic blocks, it may be more
    // convenient to just iterate over the instructions instead. It saves you
    // one level of indentation.
    //
    for (inst_iterator i = inst_begin(f); i != inst_end(f); ++i) {
      // A common pattern in function analyses is to do things depending on
      // the type of instruction that is seen. The old way of doing this is with
      // an if-else chain.
      if (auto *load = dyn_cast<LoadInst>(&*i)) {
        errs() << "Encountered a load: " << *load << "\n";
      } else if (auto *store = dyn_cast<StoreInst>(&*i)) {
        errs() << "Encountered a store: " << *store << "\n";
      }
    }

    // The "modern" approach is to use a visitor class. The InstVisitor approach
    // is not always better though - depending on what you want to do with each
    // instruction, so it's something to keep in mind.
    MyInstVisitor visitor;
    visitor.visit(f);

    // Since this is an analysis pass, other analysis results will not be
    // invalidated.
    return PreservedAnalyses::all();
  }
};

} // namespace lanlyzer

// The callback to parse the pass name when using opt. With this, one can say
//
//     opt --passes="(lanlyzer-functions)"
//
static bool passParsePipeline(StringRef name, FunctionPassManager &fpm,
                              ArrayRef<PassBuilder::PipelineElement>) {
  if (name == passPipelineName) {
    fpm.addPass(lanlyzer::FunctionAnalyzerPass());
    return true;
  }
  return false;
}

// The callback to register the pass with the pass manager. The precise
// signature of this callback will vary depending on with PassBuiler::register*
// method you use (see `registerPass` below). In this particular case, we are
// adding the pass early in the optimization pipeline. The OptimizationLevel
// argument is ignored since this is an analysis pass and we want it to always
// run. But for an optimization pass, this could be used to enable it only at
// certain optimization levels.
static void passRegisterWithPassManager(ModulePassManager &mpm,
                                        OptimizationLevel) {
  // This createModuleToFunctionPassAdaptor weirdness is one of the "features"
  // of the new pass manager. I am sure it makes sense because it may be a
  // software engineering design pattern or something, but to me, it's just a
  // pain.
  mpm.addPass(
      createModuleToFunctionPassAdaptor(lanlyzer::FunctionAnalyzerPass()));
}

// This callback is used to register the pass with the PassBuilder to allow it
// to be used in different ways. In this case, we want to be able to run it from
// with opt using ```opt --passes```, but we also want it to run automatically
// when passed to clang/flang with -fpass-plugin. There are other register
// methods in the PassBuilder that can be used to run the pass at different
// stages in the optimization pipeline. OptimizerEarlyEP is probably not the
// best place, so you should really take a look at the options to see if there
// is a better point at which this pass ought to run.
static void registerPass(PassBuilder &pb) {
  pb.registerPipelineParsingCallback(passParsePipeline);
  pb.registerOptimizerEarlyEPCallback(passRegisterWithPassManager);
}

// This is the main magic incantation that allows the pass to work automatically
// once the shared object into which this is built is loaded. The load will
// be done either by -fpass-plugin in the case of clang/flang or -load with opt.
// It is a good idea to design the pass so that it can work with both tools.
extern "C" ::llvm::PassPluginLibraryInfo llvmGetPassPluginInfo() {
  return {LLVM_PLUGIN_API_VERSION, passName, LLVM_VERSION_STRING, registerPass};
}
